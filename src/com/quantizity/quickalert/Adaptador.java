package com.quantizity.quickalert;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.Vector;

/**
 * Created by paco on 02/11/13.
 */
public class Adaptador extends BaseAdapter {
    private final Activity actividad;
    private final Vector<Alert> lista;

    public Adaptador(Activity actividad, Vector<Alert> lista) {
        super();
        this.actividad = actividad;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.elementAt(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = actividad.getLayoutInflater();
        View view = inflater.inflate(R.layout.elemento_lista, null);
        TextView textView = (TextView)view.findViewById(R.id.textoAlarm);
        textView.setText(lista.elementAt(position).getDescription());

        Button detalle = (Button) view.findViewById(R.id.detalles);
        Button cancelar = (Button) view.findViewById(R.id.cancelar);

        OnClickListener listener = new OnClickListener() {
            public void onClick(View v) {
                // TODO: llamar a actividad que muestre detalle de alarma en forma modal
            }

        };
        detalle.setOnClickListener(listener);
        OnClickListener listener2 = new OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: cancelar alarma y refrescar lista
            }
        };
        cancelar.setOnClickListener(listener2);
        return view;
    }
}
