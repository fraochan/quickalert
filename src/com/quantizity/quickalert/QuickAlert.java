package com.quantizity.quickalert;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.Date;

public class QuickAlert extends Activity {
	EditText txtHours, txtMinutes;
	ListView lista;
    SeekBar seekBar, seekBar2;
	OnFocusChangeListener listener;
    SeekBar.OnSeekBarChangeListener barListener;

	private static AlertStorage alStor = null;
	private static Context ctx = null;
	
	public static final String UPDATE_ACTION = "com.quantizity.quickalert.UPDATE_ALERT_LIST";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = getApplicationContext();
		setContentView(R.layout.activity_quick_alert);
		txtHours = (EditText) findViewById(R.id.txtHours);
		txtMinutes = (EditText) findViewById(R.id.txtMinutes);
		lista = (ListView) findViewById(R.id.lstAlerts);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
        lista.requestFocus();
		try {
			alStor = new AlertStorage();
		} catch (Exception e) {
			e.printStackTrace();
		}
        lista.setAdapter(new Adaptador(this, alStor.getAlerts()));

		txtHours.setOnFocusChangeListener(listener);
		txtMinutes.setOnFocusChangeListener(listener);
        barListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar sB, int progress, boolean fromUser) {
                if (sB.getId()==seekBar.getId()) { // horas
                    txtHours.setText(String.valueOf(progress));
                } else { // minutos
                    progress /= 1;
                    progress *= 1;
                    txtMinutes.setText(String.valueOf(progress));
                    sB.setProgress(progress);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
        seekBar.setOnSeekBarChangeListener(barListener);
        seekBar2.setOnSeekBarChangeListener(barListener);
		// arrancamos servicio de alertas
		//startService(new Intent(this, AlertService.class));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		// reload list of alerts from file
		if(alStor!=null) alStor.loadAlerts(this);
	}
	
	public static void reloadContent() {
		alStor.loadAlerts(ctx);
	}
	
	public static AlertStorage getAlertStorage() {
		if (alStor==null) {
			try {
				alStor = new AlertStorage();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		alStor.loadAlerts(ctx);
		return alStor;
	}

	public static Context getAppContext() {
		return ctx;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle savedState){
		super.onSaveInstanceState(savedState);
		savedState.putCharSequence("hours", txtHours.getText());
		savedState.putCharSequence("minutes", txtMinutes.getText());
	} 
	
	@Override
    protected void onRestoreInstanceState(Bundle savedState){
          super.onRestoreInstanceState(savedState);
          if (savedState!=null) {
          	  txtHours.setText(savedState.getCharSequence("hours").toString());
          	  txtMinutes.setText(savedState.getCharSequence("minutes").toString());
            }
	}

	
	public void activate(View view)
	{
		// Setup alarm service
		String hours = txtHours.getText().toString();
		String minutes = txtMinutes.getText().toString();
        Integer hrs = 0;
        Integer mins = 0;
		boolean pluralHr, pluralMin, setHours, setMinutes;
		
		setHours = "".equals(hours) || "0".equals(hours) ? false : true;
		setMinutes = "".equals(minutes) || "0".equals(minutes) ? false : true;
		try {
            hrs = Integer.parseInt(hours);
            pluralHr = hrs > 1 ? true : false;
		} catch (Exception ex) {
            pluralHr = false;
		}
		try {
            mins = Integer.parseInt(minutes);
            pluralMin = mins > 1 ? true : false;
		} catch (Exception ex) {
			pluralMin = false;
		}
        long timeToFire = (hrs*3600+mins*60)*1000;

        if (!setHours && !setMinutes) {
			return;
		}
		String textAlert = getResources().getString(R.string.txtAlert_begin) + " " + 
				(!setHours ? "" : txtHours.getText().toString() + (pluralHr ? " " + getResources().getString(R.string.hours) : " " + getResources().getString(R.string.hour))) + 
				(!setHours || !setMinutes? "": " " + getResources().getString(R.string.txtAlert_and) + " ") +
				(!setMinutes? "" :txtMinutes.getText().toString() + (pluralMin ? " " + getResources().getString(R.string.minutes) : " " + getResources().getString(R.string.minute)));
		
		// update list of alerts (check if alert already exists for same goal)
		
		long current = System.currentTimeMillis();
		Alert alert = new Alert();
		alert.setDuration(Long.valueOf(timeToFire));
		Date initTime = new Date();
		initTime.setTime(current);
		alert.setInitTime(initTime);
            Date endTime = new Date();
            endTime.setTime(current+timeToFire);
            alert.setEndTime(endTime);
            if (alStor.hasSameEndTime(alert)) {
                Toast.makeText(this, R.string.alert_exists_text, Toast.LENGTH_SHORT).show();
                return;
		}
		int retCode = alStor.addAlert(alert);
		if (retCode==-1) {
			Toast.makeText(this, R.string.alert_error_text, Toast.LENGTH_SHORT).show();
			return;
		} else if (retCode==-2) {
			Toast.makeText(this, R.string.alert_error_max_elements, Toast.LENGTH_SHORT).show();
			return;
		} else {
            Toast.makeText(this, textAlert, Toast.LENGTH_SHORT).show();
        }
		alStor.persistAlerts(this);
		
        AlarmManager almMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent (this, AlertActivity.class),PendingIntent.FLAG_UPDATE_CURRENT);
        almMgr.set(AlarmManager.RTC_WAKEUP,  current+timeToFire, pi);


	}
	
	public void reset(View view)
	{
		txtHours.setText("0");
		txtMinutes.setText("5");
        seekBar.setProgress(0);
        seekBar2.setProgress(5);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.AcercaDe:
			about(null);
			break;
		}
		return true;
	}
	
	public void startPreferences(View view)
	{
		// TODO: create preference activity to change preference such as ring music, snoozing, volume, etc...
	}
	
	public void about(View view)
	{
		Intent i = new Intent(this, AcercaDe.class);
		startActivity(i);
	}
}
